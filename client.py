import requests as req

def client_calls():
    """Runs Client Calls to Alphaadvantage for data"""
    response = setup_client()
    print("Stop here")
    return response


def setup_client():
    host = "https://www.alphavantage.co/query"
    params = {"function": "TIME_SERIES_DAILY", "symbol": "NVDA", "apikey": "P7AAWA9OW2049J95"}
    r = req.get(host, params)
    status = r.status_code
    body = r.json()
    return body
