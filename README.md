## Machine Learning for Trading using Python

### Working in the project
This project manages dependencies in [requirements.txt](requirements.txt).
Each required dependency is defined here and locked to a specific version.

#### Running the application
The project includes a [Makefile](Makefile) that helps setup and run the application.
The Makefile creates a Python3 virtual environment at `.venv/`.

```bash
make
```
Simply running `make` will:
1. Clean up any old environment
2. Create a new environment
3. Install required dependencies
4. Execute [financial](financial)

```bash
make init
source .venv/bin/activate
```
Running the above commands will setup your terminal for development of the application, 
allowing execution of any python3 file in the project. 

### References
[Alpha Advantage APIdocs](https://www.alphavantage.co/documentation/)

[Pandas DataFrame Join](http://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.join.html)


[Pandas DataFrame fillna)() fill forwards/backwards instead of interpolating](http://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.fillna.html)


[Matplotlib](https://matplotlib.org/)

[NumPy](http://www.numpy.org/)

[SciPy.org NumPy Documentation](https://docs.scipy.org/doc/numpy/reference/routines.math.html)

### DataLake Sources
Example alphavantage call
` curl -X GET https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=SPY&apikey=[]&datatype=csv`

#### Creating Dataframes

##### Create an Empty Dataframe
 + Helps align stock data and orders it by trading date
```python
import pandas as pd

 start_date = '2018-01-22'
 end_date = '2018-01-26'
 dates = pd.date_range(start_date, end_date)
 df = pd.DataFrame(index=dates)  
 
 print df
    
   Empty DataFrame
   Columns: []
   Index: [2010-01-22 00:00:00, 2010-01-23 00:00:00, 2010-01-24 00:00:00, 2010-01-25 00:00:00, 2010-01-26 00:00:00]
 
```
##### Create a Dataframe from CSV
```python
import pandas as pd

 dfspy = pd.read_csv("datalake/SPY.csv", index_col="timestamp", parse_dates=True,
                     usecols=['timestamp', 'high', 'low'], na_values=['nan'])
```


#### Joining Dataframes
 + Dataframe.join() defaults to left join
```python
    df = df.join(dfspy)
    df = df.dropna()
```

 + above 2 operations can be mitigated w inner join
```python
 df = df.join(dfspy, how='inner')
```

#### Slicing Dataframes
 + Slice by a row range (dates) using DataFrame.ix[] selector
```python
    df.ix['2018-01-01':'2018-01-31']
```

+ Slice by Column by passing a label(s)
```python
    df['GOOG']
    
    df[['GOOG', 'NVDA', 'AMZN']]  #list of labels selects multiple columns
```
+ Slice by Row and Column using DataFrame.ix[] selector
```python
    df.ix['2018-01-01':'2018-01-31', ['GOOG', 'NVDA', 'AMZN']]
```

#### Normalizing Price Data so all Prices Start at 1.0
+ divide the entire dataframe by it's first row, ie divide value of each column by day1 value
```python
    # df = df / df[0]
    df = df / df.ix[0, :]
    
```

#### Notes:
 + NumbPy is a python wrapper around C and Fortran, hence is very fast
 + Pandas is a wrapper around a NumPy NDArray (N-Dimensional Array)
 + Below example access underlying dnarray in a Pandas Dataframe

```python
nd = df.values  #creates an numPy ndarray out of a pandas dataframe

```

#### Notes on ndarray Syntax / Notations
```python
nd[row, col]
nd[0, 0]

```

##### ndarray Range Notation : Colon
+ Below example indicates row 0 to 2 (3 exclusive) and
column 1 to 2 (3 exclusive)

```python
nd[0:3, 1:3]

``` 

+ Below example indicates all rows, column 3
```python
nd[:, 3]

``` 

+ Special Syntax to indicate last row or column
+ Below example indicates last row, last column
```python
nd[-1, -1]

``` 

+ NumPy has an `np.array` function which can convert any array-like object 
to an ndarray
+ np.array function can take as input a list, a tuple, or array-like objects 
```python
import numpy as np

print np.array([1, 2, 3])

``` 

+ 2d Array (Sequence of Sequences)
```python
import numpy as np

print np.array([(1, 2, 3), (1, 2, 3)])

``` 

+ Create an Empty Array
```python
import numpy as np

print np.empty(5) # single-dimensional array w 5 spaces

[0.   0.25 0.5  0.75 1.  ] # floating point representation of memory data

print np.empty((rows, col)) 

```

+ Documentation for creating an array with default values
+ [numpy.ones](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ones.html)

```python
import numpy as np

print np.ones((5, 4))

[[1. 1. 1. 1.]
 [1. 1. 1. 1.]
 [1. 1. 1. 1.]
 [1. 1. 1. 1.]
 [1. 1. 1. 1.]]
 
```

+ [numpy.zeros](https://docs.scipy.org/doc/numpy/reference/generated/numpy.zeros.html)
```python
import numpy as np

print np.zeros((5, 4))

[[0. 0. 0. 0.]
 [0. 0. 0. 0.]
 [0. 0. 0. 0.]
 [0. 0. 0. 0.]
 [0. 0. 0. 0.]]


``` 

+ Default [DataTypes in ndarray](https://docs.scipy.org/doc/numpy/user/basics.types.html) are floats.
+ See below examples for specifying the datatype for each array element
+ numPy has more data types than default Python for increased accuracy
```python
import numpy as np

print np.empty(5, dtype='int_')

print np.array([1.2, 2.3, 3.9], dtype='int_')
[1 2 3]

``` 

+ Random Number Generation
+ [numpy.random.random Documentation](https://docs.scipy.org/doc/numpy/reference/generated/numpy.random.random.html)
+ Below generates an array of random numbers, uniformly sampled from [0.0, 1.0]
```python
import numpy as np

print np.random.random((5, 4)) # size tuple arg

[[0.03389551 0.60685634 0.8878409  0.70910027]
 [0.87130895 0.31944937 0.37105675 0.50508959]
 [0.79418328 0.35645714 0.8949912  0.14201404]
 [0.6166381  0.16617047 0.89326467 0.1605213 ]
 [0.2075141  0.19581237 0.47074257 0.75997977]]

```
+ To establish compatibility w Matlab syntax, below rand() does not
require a tuple as an argument:
```python
import numpy as np

print np.random.rand(5, 4)

[[0.54379583 0.72505698 0.37885526 0.171272  ]
 [0.33082682 0.52452307 0.49875971 0.24206208]
 [0.10240235 0.07432781 0.66962869 0.67739628]
 [0.02264617 0.87751847 0.99404676 0.02962761]
 [0.95610416 0.05796849 0.06425236 0.72689149]]
```

+ Sample Numbers from a [Gaussian](https://docs.scipy.org/doc/numpy/reference/generated/numpy.random.normal.html) (normal) distribution
+ Below example defaults to standard normal = (mean = 0, standard deviation = 1)
```python
import numpy as np

print np.random.normal(size=(2, 3)) 

[[ 0.44721663  0.05100555  0.03633947]
 [-1.33069526 -0.84819125 -0.90174532]]

```

+ To change the default mean = 50 and standard deviation = 10
```python
import numpy as np

mean = 50
sd = 10

print np.random.normal(mean, sd, size=(2, 3)) 

[[26.78894226 51.35880038 62.61514111]
 [48.85938123 45.83117042 46.60694577]]

```

+ Print random integers within a range using [randint](https://docs.scipy.org/doc/numpy/reference/generated/numpy.random.randint.html)
```python
import numpy as np

print np.random.randint(10) # single int in [0, 10)
0

print np.random.randint(0, 10) # same as above, low - high explicit
9

low = 10
high = 50
print np.random.randint(low, high, size=(2, 3))

[[11 15 27]
 [49 17 47]]

```
#### numPy Array Attributes

+ [numpy.ndarray.shape](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.shape.html)
```python
import numpy as np

nd_array = np.random.random((5, 4))
print nd_array

[[0.97569995 0.57805686 0.41553788 0.22245788]
 [0.133178   0.79751019 0.05153622 0.7653472 ]
 [0.78120404 0.41530438 0.96948411 0.72184774]
 [0.22260717 0.51194258 0.26395417 0.91955362]
 [0.14630441 0.19531198 0.836962   0.37110168]]
  
print nd_array.shape 

(5, 4)

print nd_array.shape[0] # number of rows
5

print nd_array.shape[1] # num of columns
4

```
+ [numpy.ndarray.ndim](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.ndim.html)
```python
import numpy as np

nd_array = np.random.random((5, 4))
print nd_array

[[0.97569995 0.57805686 0.41553788 0.22245788]
 [0.133178   0.79751019 0.05153622 0.7653472 ]
 [0.78120404 0.41530438 0.96948411 0.72184774]
 [0.22260717 0.51194258 0.26395417 0.91955362]
 [0.14630441 0.19531198 0.836962   0.37110168]]
  
print nd_array.ndim 

2

nd_array = np.random.random((5, 4, 3))

print nd_array.ndim 

3

```

+ [numpy.ndarray.size](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.size.html)
+ [numpy.ndarray.dtype](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.dtype.html)
```python
import numpy as np

nd_array = np.random.random((2, 4, 3))
print nd_array # 2 4x3 arrays

[[[0.77071895 0.66721666 0.51023674]
  [0.83884953 0.18807134 0.72576357]
  [0.21663263 0.97394616 0.10632678]
  [0.89146437 0.52707943 0.74139631]]

 [[0.8606267  0.08855731 0.85146983]
  [0.81073052 0.52895923 0.53780133]
  [0.41974441 0.45769833 0.26767655]
  [0.93792514 0.95266089 0.96479484]]]
  
print nd_array.size 

24

print nd_array.dtype

float64

```
#### numPy Mathematical Functions
+ [SciPy.org Documentation](https://docs.scipy.org/doc/numpy/reference/routines.math.html)
+ Re-executing randint with the seed will always produce the same sequence [2, 0, 5, 1 . . .]
+ [Sum all elements in an array](https://docs.scipy.org/doc/numpy/reference/generated/numpy.sum.html)
+ Sum a direction (Axis) of the array. `axis=0` signifies traversal across row elements, `axis=1` across column elements

```python
import numpy as np

low = 0
high = 10
np.random.seed(693)
ary = np.random.randint(low, high, size=(5, 4))

row_axis = 0
column_axis = 1
row_iterate = ary.sum(row_axis)  # ary.sum(axis=0)
column_iterate = ary.sum(column_axis) # ary.sum(axis=1)

print row_iterate  # prints sum of each column
print column_iterate # prints sum of each row
print ary.sum()
    
[[2 0 5 1]
 [1 3 4 4]
 [9 2 9 1]
 [9 3 7 5]
 [4 7 0 3]]
 
[25 15 25 14]  # column sum
[ 8 12 21 24 14]  # row sum

79  # sum of all elements

```
+ Statistics: min, max, [mean](https://docs.scipy.org/doc/numpy/reference/generated/numpy.mean.html) (across rows, cols, and overall
```python
import numpy as np

low = 0
high = 10
np.random.seed(693)
ary = np.random.randint(low, high, size=(5, 4))

min = ary.min()
max = ary.max()
mean = ary.mean()

min_c = ary.min(axis=0)
max_c = ary.max(axis=0)
mean_c = ary.mean(axis=0)

min_r = ary.min(axis=1)
max_r = ary.max(axis=1)
mean_r = ary.mean(axis=1)

print("Minimum: {}".format(min))
print("Maximum: {}".format(max))
print("Mean: {}".format(mean))

print("Minimum of each column: {}".format(min_c))
print("Maximum of each column: {}".format(max_c))
print("Mean of each column: {}".format(mean_c))

print("Minimum of each row: {}".format(min_r))
print("Maximum of each row: {}".format(max_r))
print("Mean of each row: {}".format(mean_r))

```

+ [Integer Array Indexing](https://docs.scipy.org/doc/numpy/reference/arrays.indexing.html#integer-array-indexing)
```python
import numpy as np

a = [[2 0 5 1]
     [1 3 4 4]
     [9 2 9 1]
     [9 3 7 5]
     [4 7 0 3]]

indices = np.array([1, 1, 2, 3])
b = a[indices]

print b

[[1 3 4 4]  # row 1
 [1 3 4 4]  # row 1
 [9 2 9 1]  # row 2
 [9 3 7 5]] # row 3
 
c = [1, 3, 8, 7] # single-dimensional 
d = c[indices] 

print d

[3, 3, 8, 7]
 
```

+ Filtering Against Boolean Array
+ Note: The expression `a < mean` produces a boolean array, like: `[[False, False, True, False, False, False, True, True, True],
 [True, True, False, False, True, True, False, True, True]]`

```python

a = np.array([(20, 25, 10, 23, 26, 32, 10, 5, 0),
                               (0, 2, 50, 20, 0, 1, 28, 5, 0)])
mean = a.mean()
filtered = a[a < mean]

[10 10  5  0  0  2  0  1  5  0]
```

##### Forward Fill vs Interpolating Data
+ Forward Fill First
+ Backwards Fill 2nd
+ Do Not Interpolate Data

```python
df.fillna(method='ffill', inplace=True)  
```