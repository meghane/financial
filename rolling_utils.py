import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as matplt
import numpy as np
import dataframe_utils as dfu


def plot_rolling_data(df, symbol, window):
    # TODO validate window < dates
    font = {'family': 'normal',
            'weight': 'bold',
            'size': 6}
    matplt.rc('font', **font)

    # compute rolling mean of dataframe
    rm = calculate_rolling_mean(df[symbol], window)
    rstd = calculate_rolling_std(df[symbol], window)
    upper_band, lower_band = calculate_bollinger_bands(rm, rstd)

    # create matplotlib axis object from our dataframe
    ax = df[symbol].plot(title="MegRyan Simple Moving Average and Bollinger Bands", fontsize=6)

    # add rolling mean, bollinger_bands to ax plot
    rm.plot(label="Rolling Mean", ax=ax)
    upper_band.plot(label="Upper Band", ax=ax)
    lower_band.plot(label="Lower Band", ax=ax)

    ax.set_xlabel("Date")
    ax.set_ylabel("Price")
    ax.legend(loc='upper left')
    plt.show()


def plot_daily_returns(df, symbol):
    font = {'family': 'normal',
            'weight': 'bold',
            'size': 6}
    matplt.rc('font', **font)
    ax = df[symbol].plot(title="MegRyan Daily Returns", fontsize=6)
    ax.set_ylabel("Daily Returns")


def plot_histogram(df, bins):
    mean = df.mean(skipna=True)
    std = df.std(skipna=True)
    df.hist(bins=bins)
    plt.axvline(mean.item(), color='w', linestyle='dashed', linewidth=2)
    plt.axvline(std.item(), color='r', linestyle='dashed', linewidth=1)
    plt.axvline(mean.item()-std.item(), color='r', linestyle='dashed', linewidth=1)


def plot_histograms(df, bins):
    # This dataframe requires 2 columns. In this case NVDA and SPY
    df['NVDA'].hist(bins=bins, label="NVDA")
    df['SPY'].hist(bins=bins, label="SPY")
    plt.legend(loc='upper right')
    mean_NVDA = df['NVDA'].mean(skipna=True)
    mean_SPY = df['SPY'].mean(skipna=True)
    std_NVDA = df['NVDA'].std(skipna=True)
    std_SPY = df['SPY'].std(skipna=True)

    print(mean_NVDA.item())
    print(mean_SPY.item())

    plt.axvline(mean_NVDA.item(), color='r', linestyle='dashed', linewidth=2)
    plt.axvline(std_NVDA.item(), color='r', linewidth=2)
    plt.axvline(-std_NVDA.item(), color='r', linewidth=2)

    plt.axvline(mean_SPY.item(), color='y', linestyle='dashed', linewidth=2)
    plt.axvline(std_SPY.item(), color='y', linewidth=2)
    plt.axvline(-std_SPY.item(), color='y', linewidth=2)


def plot_scatterplot(df):
    # This dataframe requires 2 columns. In this case NVDA and SPY
    df.plot(kind='scatter', x='SPY', y='NVDA')
    # np.polyfit takes a series of points to generate a line for x (in this case, NVDA) and the same for y (SPY)
    betaNVDA, alphaNVDA = np.polyfit(df['SPY'].dropna().values, df['NVDA'].dropna().values, 1) # 1 indicated polynomial with degree 1
    print("Nvidia Beta is {} while Nvidia alpha is {}".format(betaNVDA, alphaNVDA))
    plt.plot(df['SPY'], betaNVDA * df['SPY'] + alphaNVDA, '-', color='r')



def render_plots():
    plt.show()


def calculate_rolling_mean(df, window):
    return pd.DataFrame.rolling(df, window=window).mean()


def calculate_rolling_std(df, window):
    return pd.DataFrame.rolling(df, window=window).std()


def calculate_bollinger_bands(rolling_mean_df, rolling_std_df):
    upper_band = rolling_mean_df + rolling_std_df * 2
    lower_band = rolling_mean_df - rolling_std_df * 2
    return upper_band, lower_band


def calculate_daily_returns(df):
    # daily_return = (today / yesterday) -1
    daily_returns = df.copy()
    daily_returns[1:] = (df[1:] / df[:-1].values) - 1
    daily_returns.ix[0] = 0
    return daily_returns


def calculate_daily_returns_pandas(df):
    daily_returns = (df / df.shift(1)) -1
    daily_returns.ix[0] = 0
    return daily_returns

