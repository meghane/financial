def df_tail(df):
    """Prints Last N Lines of a Dataframe"""
    print("\n --- Tail --- ")
    print(df.tail(5))


def df_head(df):
    """Prints First N Lines of a Dataframe"""
    print("\n --- Head --- ")
    print(df.head(5))


def df_range(df):
    """Prints lines within given range of a Dataframe"""
    print("\n --- Range --- ")
    print(df[25:30])


def df_keys(df):
    print("\n --- Keys --- ")
    keys = list(df.columns.values)
    for header in keys:
        print(header)


def vol_mean(symbol, df):
    """Obtain the volume mean using Pandas"""
    mean = df['volume'].mean()
    print(symbol, mean)


def calculate_mean(df):
    """Obtain the mean using Pandas Dataframe"""
    return df.mean()


def calculate_median(df):
    """Obtain the median value of a sorted collection using Pandas Dataframe"""
    return df.median()


def calculate_standard_deviation(df):
    """Obtain the standard deviation using Pandas Dataframe"""
    return df.std()