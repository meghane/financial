import numpy as np
import time

def np_run():
    t1 = time.time()
    np_array = numpy_demo_setup()
    numpy_statistics_example(np_array)
    index_array_with_other_array_example(np_array)
    array_arithmetic_operations()
    numpy_array_assignment_example(np_array)
    boolean_arrays_mask_example()
    t2 = time.time()
    print("Duration was {} seconds.".format(t2 - t1))
    

def numpy_demo_setup():
    low = 0
    high = 10
    np.random.seed(693)
    nd_array = np.random.randint(low, high, size=(5, 4))
    print(nd_array)
    return nd_array


""" Statistics: min, max, mean"""


def numpy_statistics_example(a):
    min_a = a.min()
    max_a = a.max()
    mean_a = a.mean()

    min_c = a.min(axis=0)
    max_c = a.max(axis=0)
    mean_c = a.mean(axis=0)

    min_r = a.min(axis=1)
    max_r = a.max(axis=1)
    mean_r = a.mean(axis=1)

    print("Minimum: {}".format(min_a))
    print("Maximum: {}".format(max_a))
    print("Mean: {}".format(mean_a))

    print("Minimum of each column: {}".format(min_c))
    print("Maximum of each column: {}".format(max_c))
    print("Mean of each column: {}".format(mean_c))

    print("Minimum of each row: {}".format(min_r))
    print("Maximum of each row: {}".format(max_r))
    print("Mean of each row: {}".format(mean_r))
    
    
def numpy_array_assignment_example(a):
    a[0, 0] = 1
    a[1, :] = 7
    print(a)


def index_array_with_other_array_example(a):
    indices = np.array([1, 1, 2, 3])
    print(indices)
    b = a[indices]
    print(b)
    print(a[indices])


"""NumPy Masking is Equivalent to Functional Filtering of a Stream"""


def boolean_arrays_mask_example():
    b = np.array([(20, 25, 10, 23, 26, 32, 10, 5, 0),
                               (0, 2, 50, 20, 0, 1, 28, 5, 0)])
    mean = b.mean()
    filtered = b[b < mean]
    print(filtered)
    print("Filter out elements less than the mean of {}. The results are {}".format(mean, filtered))


def array_arithmetic_operations():
    c = np.array([(1, 2, 3, 4, 5), (10, 20, 30, 40, 50)])
    print("Initial Array is {}".format(c))

    d = 2 * c
    print("Multiply by 2 Generates new Array {}".format(d))
